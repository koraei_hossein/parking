package com.koraei.parking.api.controller;

import com.koraei.parking.api.model.vehicle.VehicleAddModel;
import com.koraei.parking.api.utils.helper.ResponseHelper;
import com.koraei.parking.service.VehicleService;
import com.koraei.parking.service.domain.VehicleResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value = "vehicle-service/")
public class VehicleController {

  @Autowired
  VehicleService vehicleService;

  @ApiOperation(value = "", response = VehicleResponse.class)
  @RequestMapping(value = "/add", method = RequestMethod.POST)
  public ResponseEntity add(@RequestBody VehicleAddModel model) {
    return ResponseHelper.response(
        vehicleService.add(
            model.getCar_tag(),
            model.getOwnerName(),
            model.getModel()
        ));
  }

}
