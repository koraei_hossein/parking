package com.koraei.parking.api.controller;

import com.koraei.parking.api.model.price.PriceAddModel;
import com.koraei.parking.api.utils.helper.ResponseHelper;
import com.koraei.parking.service.PriceRateService;
import com.koraei.parking.service.domain.PriceRateResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value = "price-rate-service/")
public class PriceController {

  @Autowired
  PriceRateService priceRateService;

  @ApiOperation(value = "", response = PriceRateResponse.class)
  @RequestMapping(value = "/add", method = RequestMethod.POST)
  public ResponseEntity add(@RequestBody PriceAddModel model) {
    return ResponseHelper.response(
        priceRateService.add(
            model.getHourly(),
            model.getDaily(),
            model.getMonthly(),
            model.getActive()
        ));
  }

}
