package com.koraei.parking.api.controller;

import com.koraei.parking.api.model.parking.ParkingAddModel;
import com.koraei.parking.api.model.price.PriceAddModel;
import com.koraei.parking.api.utils.helper.ResponseHelper;
import com.koraei.parking.service.ParkingService;
import com.koraei.parking.service.domain.ParkingResponse;
import com.koraei.parking.service.domain.PriceRateResponse;
import io.swagger.annotations.ApiOperation;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
@RequestMapping(value = "parking-service/")
public class ParkingController {

  @Autowired
  ParkingService parkingService;

  @ApiOperation(value = "", response = ParkingResponse.class)
  @RequestMapping(value = "/add", method = RequestMethod.POST)
  public ResponseEntity add(@RequestBody ParkingAddModel model) {
    return ResponseHelper.response(
        parkingService.insertAndUpdate(
            model.getCarTag()
        ));
  }

  @ApiOperation(value = "", response = ParkingResponse.class)
  @RequestMapping(value = "/get-vehicle-report", method = RequestMethod.GET)
  public ResponseEntity add(
      @RequestParam String carTag,
      @RequestParam String logInTime) {

    DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    LocalDateTime date = dateTimeFormat.parse(logInTime, LocalDateTime::from);

    return ResponseHelper.response(
        parkingService.getAllVehicleReport(carTag, date));
  }


}
