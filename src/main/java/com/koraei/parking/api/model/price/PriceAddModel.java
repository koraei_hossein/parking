package com.koraei.parking.api.model.price;

import java.math.BigDecimal;

public class PriceAddModel {

  private BigDecimal hourly;
  private BigDecimal daily;
  private BigDecimal monthly;
  private Boolean active;

  public BigDecimal getHourly() {
    return hourly;
  }

  public BigDecimal getDaily() {
    return daily;
  }

  public BigDecimal getMonthly() {
    return monthly;
  }

  public Boolean getActive() {
    return active;
  }
}
