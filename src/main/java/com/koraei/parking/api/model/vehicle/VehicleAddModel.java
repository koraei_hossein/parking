package com.koraei.parking.api.model.vehicle;

public class VehicleAddModel {

  private String car_tag;
  private String ownerName;
  private String model;

  public String getCar_tag() {
    return car_tag;
  }

  public String getOwnerName() {
    return ownerName;
  }

  public String getModel() {
    return model;
  }
}
