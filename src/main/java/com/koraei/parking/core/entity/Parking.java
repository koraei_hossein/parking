package com.koraei.parking.core.entity;

import com.koraei.parking.core.constant.Payment;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_parking")
public class Parking {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "log_in_date", nullable = false)
  private LocalDateTime logInDate;

  @Column(name = "log_out_date")
  private LocalDateTime logOutDate;

  @Column(name = "price", nullable = false)
  private BigDecimal price;

  @Column(name = "payment_state")
  @Enumerated(EnumType.STRING)
  private Payment payment;

  @ManyToOne(fetch = FetchType.LAZY, targetEntity = Vehicle.class)
  @JoinColumn(name = "vehicle_id", foreignKey = @ForeignKey(name = "FK_VEHICLE_PARKING"),
      nullable = false)
  private Vehicle vehicle;

  @ManyToOne(fetch = FetchType.LAZY, targetEntity = PriceRate.class)
  @JoinColumn(name = "price_rate_id", foreignKey = @ForeignKey(name = "FK_PRICE_RATE_PARKING"),
      nullable = false)
  private PriceRate priceRate;

  public Parking(
      LocalDateTime logInDate,
      LocalDateTime logOutDate,
      BigDecimal price,
      Payment payment,
      Vehicle vehicle,
      PriceRate priceRate) {
    this.logInDate = logInDate;
    this.logOutDate = logOutDate;
    this.price = price;
    this.payment = payment;
    this.vehicle = vehicle;
    this.priceRate = priceRate;
  }

  public Parking() {
  }

  public long getId() {
    return id;
  }

  public LocalDateTime getLogInDate() {
    return logInDate;
  }

  public LocalDateTime getLogOutDate() {
    return logOutDate;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public Vehicle getVehicle() {
    return vehicle;
  }

  public PriceRate getPricaRate() {
    return priceRate;
  }

  public Payment getPayment() {
    return payment;
  }


  public void setId(long id) {
    this.id = id;
  }

  public void setLogInDate(LocalDateTime logInDate) {
    this.logInDate = logInDate;
  }

  public void setLogOutDate(LocalDateTime logOutDate) {
    this.logOutDate = logOutDate;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  public void setPriceRate(PriceRate priceRate) {
    this.priceRate = priceRate;
  }

  public void setPayment(Payment payment) {
    this.payment = payment;
  }
}
