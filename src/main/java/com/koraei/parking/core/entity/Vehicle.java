package com.koraei.parking.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_vehicle")
public class Vehicle {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "cat_tag", nullable = false)
  private String car_tag;


  @Column(name = "owner_name", nullable = true)
  private String ownerName;

  @Column(name = "model", nullable = true)
  private String model;

  public Vehicle() {
  }

  public Vehicle(
      String car_tag,
      String ownerName,
      String model) {
    this.car_tag = car_tag;
    this.ownerName = ownerName;
    this.model = model;
  }

  public long getId() {
    return id;
  }

  public String getCar_tag() {
    return car_tag;
  }

  public String getOwnerName() {
    return ownerName;
  }

  public String getModel() {
    return model;
  }
}
