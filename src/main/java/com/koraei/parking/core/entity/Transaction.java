package com.koraei.parking.core.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_transaction_log")
public class Transaction {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @ManyToOne(fetch = FetchType.LAZY, targetEntity = Parking.class)
  @JoinColumn(name = "parking_id", foreignKey = @ForeignKey(name = "FK_PARKING_TRANSACTION"),
      nullable = false)
  private Parking parking;

  @Column(name = "creation_date", nullable = false)
  private LocalDateTime creationDate;

  @Column(name = "verify", nullable = false)
  private Boolean verify;

  public Transaction(
      Parking parking,
      LocalDateTime creationDate,
      Boolean verify) {
    this.parking = parking;
    this.creationDate = creationDate;
    this.verify = verify;
  }

  public Transaction() {
  }

  public long getId() {
    return id;
  }

  public Parking getParking() {
    return parking;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public Boolean getVerify() {
    return verify;
  }
}
