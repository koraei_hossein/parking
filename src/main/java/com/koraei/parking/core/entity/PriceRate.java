package com.koraei.parking.core.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_price_rate")
public class PriceRate {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "hourly_rate", nullable = false)
  private BigDecimal hourly;

  @Column(name = "daily_rate", nullable = false)
  private BigDecimal daily;

  @Column(name = "monthly_rate", nullable = false)
  private BigDecimal monthly;

  @Column(name = "active", nullable = false)
  private Boolean active;

  @Column(name = "creation_date", nullable = false)
  private LocalDateTime creationDate;

  public PriceRate(
      BigDecimal hourly,
      BigDecimal daily,
      BigDecimal monthly,
      Boolean active,
      LocalDateTime creationDate) {
    this.hourly = hourly;
    this.daily = daily;
    this.monthly = monthly;
    this.active = active;
    this.creationDate = creationDate;
  }

  public PriceRate() {
  }

  public long getId() {
    return id;
  }

  public BigDecimal getHourly() {
    return hourly;
  }

  public BigDecimal getDaily() {
    return daily;
  }

  public BigDecimal getMonthly() {
    return monthly;
  }

  public Boolean getActive() {
    return active;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }
}
