package com.koraei.parking.core.repository;

import com.koraei.parking.core.entity.Transaction;

public interface TransactionRepository {

  Transaction save(Transaction transaction);
}
