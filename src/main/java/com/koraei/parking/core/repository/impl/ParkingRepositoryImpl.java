package com.koraei.parking.core.repository.impl;

import com.koraei.parking.core.entity.Parking;
import com.koraei.parking.core.repository.ParkingRepository;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class ParkingRepositoryImpl extends CustomRepository implements ParkingRepository {


  @Override
  public Parking save(Parking parking) {
    return save(Parking.class, parking);
  }

  @Override
  public Parking findVehicleLogIn(String carTag) {
    return findQueryWrapper(entityManager
        .createQuery("select p from Parking p where p.vehicle.car_tag= :carTag "
                + "AND p.logOutDate IS NULL ",
            Parking.class)
        .setParameter("carTag", carTag));
  }

  @Override
  public Parking findById(long id) {
    return findById(Parking.class, id);
  }

  @Override
  public List<Parking> findAllVehicleReport(String carTag, LocalDateTime logInDate) {
    return listQueryWrapper(
        entityManager
            .createQuery(
                "select p from Parking p where p.vehicle.car_tag = :carTag "
                    + " and p.logInDate > :logInDate",
                Parking.class)
            .setParameter("carTag", carTag)
            .setParameter("logInDate", logInDate)
    );
  }
}
