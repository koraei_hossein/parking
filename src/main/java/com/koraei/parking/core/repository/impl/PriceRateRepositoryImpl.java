package com.koraei.parking.core.repository.impl;

import com.koraei.parking.core.entity.PriceRate;
import com.koraei.parking.core.entity.Vehicle;
import com.koraei.parking.core.repository.PriceRateRepository;
import org.springframework.stereotype.Repository;

@Repository
public class PriceRateRepositoryImpl extends CustomRepository implements PriceRateRepository {


  @Override
  public PriceRate save(PriceRate priceRate) {
    return save(PriceRate.class, priceRate);
  }

  @Override
  public PriceRate findById(long id) {
    return findById(PriceRate.class, id);
  }

  @Override
  public PriceRate findActivePrice() {
    return findQueryWrapper(entityManager
        .createQuery("select p from PriceRate p where p.active = true",
            PriceRate.class));
  }
}
