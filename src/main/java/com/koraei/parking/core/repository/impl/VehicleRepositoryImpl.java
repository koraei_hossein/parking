package com.koraei.parking.core.repository.impl;

import com.koraei.parking.core.entity.Vehicle;
import com.koraei.parking.core.repository.VehicleRepository;
import org.springframework.stereotype.Repository;

@Repository
public class VehicleRepositoryImpl extends CustomRepository implements VehicleRepository {

  @Override
  public Vehicle save(Vehicle vehicle) {
    return save(Vehicle.class, vehicle);
  }

  @Override
  public Vehicle findByCarTag(String carTag) {
    return findQueryWrapper(entityManager
        .createQuery("select v from Vehicle v where v.car_tag = :carTag",
            Vehicle.class)
        .setParameter("carTag", carTag));
  }

}
