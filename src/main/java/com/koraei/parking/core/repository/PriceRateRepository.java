package com.koraei.parking.core.repository;

import com.koraei.parking.core.entity.PriceRate;

public interface PriceRateRepository {

  PriceRate save(PriceRate priceRate);

  PriceRate findById(long id);

  PriceRate findActivePrice();
}
