package com.koraei.parking.core.repository;

import com.koraei.parking.core.entity.Vehicle;

public interface VehicleRepository {

  Vehicle save(Vehicle vehicle);

  Vehicle findByCarTag(String carTag);

}
