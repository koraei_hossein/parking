package com.koraei.parking.core.repository;

import com.koraei.parking.core.entity.Parking;
import com.koraei.parking.core.entity.Vehicle;
import java.time.LocalDateTime;
import java.util.List;

public interface ParkingRepository{

  Parking save(Parking parking);

  Parking findVehicleLogIn(String carTag);

  Parking findById(long id);

  List<Parking> findAllVehicleReport(String carTag, LocalDateTime logInDate);
}
