package com.koraei.parking.core.repository.impl;

import com.koraei.parking.core.entity.Transaction;
import com.koraei.parking.core.repository.TransactionRepository;
import org.springframework.stereotype.Repository;

@Repository
public class TransactionRepositoryImpl extends CustomRepository implements TransactionRepository {


  @Override
  public Transaction save(Transaction transaction) {
    return save(Transaction.class , transaction);
  }
}
