package com.koraei.parking.service;

import com.koraei.parking.service.domain.TransactionResponse;

public interface TransactionService {

  TransactionResponse add(long parkingId);
}
