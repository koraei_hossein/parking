package com.koraei.parking.service.exception;

import org.springframework.http.HttpStatus;

public class PaymentException extends CustomException{

  public PaymentException() {
    super("payment_failed", HttpStatus.NOT_ACCEPTABLE);
  }
}
