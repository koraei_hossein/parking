package com.koraei.parking.service.exception;

import org.springframework.http.HttpStatus;

public class VehicleExistException extends CustomException{

  public VehicleExistException() {
    super("vehicle_existed", HttpStatus.NOT_ACCEPTABLE);
  }
}
