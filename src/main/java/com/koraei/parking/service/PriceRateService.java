package com.koraei.parking.service;

import com.koraei.parking.core.entity.PriceRate;
import com.koraei.parking.service.domain.PriceRateResponse;
import java.math.BigDecimal;

public interface PriceRateService {

  PriceRateResponse add(
      BigDecimal hourly,
      BigDecimal daily,
      BigDecimal monthly,
      Boolean active);

  PriceRateResponse findById(long id);

  PriceRate findByIdObject(long id);

  PriceRate findActivePrice();
}
