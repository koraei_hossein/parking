package com.koraei.parking.service.domain;

import java.time.LocalDateTime;

public class TransactionResponse {

  private long id;
  private String carTag;
  private Boolean verify;
  private LocalDateTime creationTime;

  public TransactionResponse(
      long id,
      String carTag,
      Boolean verify,
      LocalDateTime creationTime) {
    this.id = id;
    this.carTag = carTag;
    this.verify = verify;
    this.creationTime = creationTime;
  }

  public long getId() {
    return id;
  }

  public String getCarTag() {
    return carTag;
  }

  public Boolean getVerify() {
    return verify;
  }

  public LocalDateTime getCreationTime() {
    return creationTime;
  }
}
