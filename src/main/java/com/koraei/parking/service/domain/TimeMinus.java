package com.koraei.parking.service.domain;

public class TimeMinus {

  private long hour;
  private long day;
  private long month;

  public TimeMinus(long hour, long day, long month) {
    this.hour = hour;
    this.day = day;
    this.month = month;
  }

  public long getHour() {
    return hour;
  }

  public long getDay() {
    return day;
  }

  public long getMonth() {
    return month;
  }
}
