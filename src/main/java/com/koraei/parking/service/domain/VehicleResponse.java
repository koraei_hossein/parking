package com.koraei.parking.service.domain;

public class VehicleResponse {

  private long id;
  private String car_tag;
  private String ownerName;
  private String model;

  public VehicleResponse(
      long id,
      String car_tag,
      String ownerName,
      String model) {
    this.id = id;
    this.car_tag = car_tag;
    this.ownerName = ownerName;
    this.model = model;
  }

  public String getCar_tag() {
    return car_tag;
  }

  public String getOwnerName() {
    return ownerName;
  }

  public String getModel() {
    return model;
  }

  public long getId() {
    return id;
  }
}
