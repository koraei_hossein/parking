package com.koraei.parking.service.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Column;

public class PriceRateResponse {

  private long id;

  private BigDecimal hourly;

  private BigDecimal daily;

  private BigDecimal monthly;

  private Boolean active;

  private LocalDateTime creationDate;

  public PriceRateResponse(
      long id,
      BigDecimal hourly,
      BigDecimal daily,
      BigDecimal monthly,
      Boolean active,
      LocalDateTime creationDate) {
    this.id = id;
    this.hourly = hourly;
    this.daily = daily;
    this.monthly = monthly;
    this.active = active;
    this.creationDate = creationDate;
  }

  public long getId() {
    return id;
  }

  public BigDecimal getHourly() {
    return hourly;
  }

  public BigDecimal getDaily() {
    return daily;
  }

  public BigDecimal getMonthly() {
    return monthly;
  }

  public Boolean getActive() {
    return active;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }
}
