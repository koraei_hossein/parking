package com.koraei.parking.service.domain;

import com.koraei.parking.core.constant.Payment;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ParkingResponse {

  private long id;
  private LocalDateTime logInDate;
  private LocalDateTime logOutDate;
  private BigDecimal price;
  private String carTag;
  private String carOwner;
  private String carModel;
  private BigDecimal priceRate;
  private TimeMinus parkTime;
  private Payment paymentStatus;


  public ParkingResponse(long id, LocalDateTime logInDate, LocalDateTime logOutDate,
      BigDecimal price, String carTag, String carOwner, String carModel,
      BigDecimal priceRate, TimeMinus parkTime,
      Payment paymentStatus) {
    this.id = id;
    this.logInDate = logInDate;
    this.logOutDate = logOutDate;
    this.price = price;
    this.carTag = carTag;
    this.carOwner = carOwner;
    this.carModel = carModel;
    this.priceRate = priceRate;
    this.parkTime = parkTime;
    this.paymentStatus = paymentStatus;
  }

  public long getId() {
    return id;
  }

  public LocalDateTime getLogInDate() {
    return logInDate;
  }

  public LocalDateTime getLogOutDate() {
    return logOutDate;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public String getCarTag() {
    return carTag;
  }

  public String getCarOwner() {
    return carOwner;
  }

  public String getCarModel() {
    return carModel;
  }

  public BigDecimal getPriceRate() {
    return priceRate;
  }

  public TimeMinus getParkTime() {
    return parkTime;
  }

  public Payment getPaymentStatus() {
    return paymentStatus;
  }
}
