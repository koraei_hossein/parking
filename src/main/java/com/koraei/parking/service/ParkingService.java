package com.koraei.parking.service;

import com.koraei.parking.core.entity.Parking;
import com.koraei.parking.service.domain.ParkingResponse;
import java.time.LocalDateTime;
import java.util.List;

public interface ParkingService {

  ParkingResponse insertAndUpdate(String carTag);

  List<ParkingResponse> getAllVehicleReport(String carTag, LocalDateTime logIn);

  Parking getByIdObject(long id);
}
