package com.koraei.parking.service.mapper;

import com.koraei.parking.core.entity.Transaction;
import com.koraei.parking.service.domain.TransactionResponse;

public class TransactionMapper {

  public static TransactionResponse toResponse(Transaction transaction){
    return new TransactionResponse(
        transaction.getId(),
        transaction.getParking().getVehicle().getCar_tag(),
        transaction.getVerify(),
        transaction.getCreationDate());
  }
}
