package com.koraei.parking.service.mapper;

import com.koraei.parking.core.entity.Vehicle;
import com.koraei.parking.service.domain.VehicleResponse;

public class VehicleMapper {

  public static VehicleResponse toResponse(Vehicle vehicle){
    return new VehicleResponse(
        vehicle.getId(),
        vehicle.getCar_tag(),
        vehicle.getOwnerName(),
        vehicle.getModel());
  }
}
