package com.koraei.parking.service.mapper;

import com.koraei.parking.core.entity.Parking;
import com.koraei.parking.service.domain.ParkingResponse;
import com.koraei.parking.service.domain.TimeMinus;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ParkingMapper {

  public static ParkingResponse toResponse(
      Parking parking,
      BigDecimal priceRate,
      TimeMinus parkTime){
    return new ParkingResponse(
        parking.getId(),
        parking.getLogInDate(),
        parking.getLogOutDate(),
        parking.getPrice(),
        parking.getVehicle().getCar_tag(),
        parking.getVehicle().getOwnerName(),
        parking.getVehicle().getModel(),
        priceRate,
        parkTime,
        parking.getPayment());
  }
}
