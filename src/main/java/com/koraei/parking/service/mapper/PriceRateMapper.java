package com.koraei.parking.service.mapper;

import com.koraei.parking.core.entity.PriceRate;
import com.koraei.parking.service.domain.PriceRateResponse;

public class PriceRateMapper {

  public static PriceRateResponse toResponse(PriceRate priceRate){
    return new PriceRateResponse(
        priceRate.getId(),
        priceRate.getHourly(),
        priceRate.getDaily(),
        priceRate.getMonthly(),
        priceRate.getActive(),
        priceRate.getCreationDate());
  }
}
