package com.koraei.parking.service;

import com.koraei.parking.core.entity.Vehicle;
import com.koraei.parking.service.domain.VehicleResponse;
import java.util.List;

public interface VehicleService {

  VehicleResponse add(
       String car_tag,
       String ownerName,
       String model);

  VehicleResponse findByCarTag(String carTag);

  Vehicle findByCarTagObject(String carTag);

  List<VehicleResponse> findAll();
}
