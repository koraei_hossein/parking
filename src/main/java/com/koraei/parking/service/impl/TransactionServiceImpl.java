package com.koraei.parking.service.impl;

import com.koraei.parking.core.entity.Parking;
import com.koraei.parking.core.entity.Transaction;
import com.koraei.parking.core.repository.TransactionRepository;
import com.koraei.parking.service.ParkingService;
import com.koraei.parking.service.TimeService;
import com.koraei.parking.service.TransactionService;
import com.koraei.parking.service.domain.TransactionResponse;
import com.koraei.parking.service.mapper.TransactionMapper;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransactionServiceImpl implements TransactionService {

  @Autowired
  TransactionRepository transactionRepository;

  @Autowired
  ParkingService parkingService;

  @Autowired
  TimeService timeService;

  @Override
  @Transactional
  public TransactionResponse add(long parkingId) {

    Parking parking = parkingService.getByIdObject(parkingId);

    Transaction transaction = new Transaction(
        parking,
        timeService.now(),
        getRandomBoolean()
    );

    transactionRepository.save(transaction);

    return TransactionMapper.toResponse(transaction);
  }

  private boolean getRandomBoolean() {
    Random random = new Random();
    return random.nextBoolean();
  }
}
