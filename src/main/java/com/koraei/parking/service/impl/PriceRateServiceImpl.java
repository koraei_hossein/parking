package com.koraei.parking.service.impl;

import com.koraei.parking.core.entity.PriceRate;
import com.koraei.parking.core.repository.PriceRateRepository;
import com.koraei.parking.service.PriceRateService;
import com.koraei.parking.service.TimeService;
import com.koraei.parking.service.domain.PriceRateResponse;
import com.koraei.parking.service.mapper.PriceRateMapper;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PriceRateServiceImpl implements PriceRateService {

  @Autowired
  PriceRateRepository priceRateRepository;

  @Autowired
  TimeService timeService;

  @Override
  @Transactional
  public PriceRateResponse add(
      BigDecimal hourly,
      BigDecimal daily,
      BigDecimal monthly,
      Boolean active) {

    PriceRate priceRate = new PriceRate(
        hourly,
        daily,
        monthly,
        active,
        timeService.now());

    return PriceRateMapper.toResponse(priceRateRepository.save(priceRate));
  }

  @Override
  public PriceRateResponse findById(long id) {
    return null;
  }

  @Override
  public PriceRate findByIdObject(long id) {
    return priceRateRepository.findById(id);
  }

  @Override
  public PriceRate findActivePrice() {
    return priceRateRepository.findActivePrice();
  }
}
