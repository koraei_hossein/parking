package com.koraei.parking.service.impl;

import com.koraei.parking.core.constant.Payment;
import com.koraei.parking.core.entity.Parking;
import com.koraei.parking.core.entity.PriceRate;
import com.koraei.parking.core.entity.Vehicle;
import com.koraei.parking.core.repository.ParkingRepository;
import com.koraei.parking.service.ParkingService;
import com.koraei.parking.service.PriceRateService;
import com.koraei.parking.service.TimeService;
import com.koraei.parking.service.TransactionService;
import com.koraei.parking.service.VehicleService;
import com.koraei.parking.service.domain.ParkingResponse;
import com.koraei.parking.service.domain.TimeMinus;
import com.koraei.parking.service.domain.VehicleResponse;
import com.koraei.parking.service.exception.NotFoundException;
import com.koraei.parking.service.exception.PaymentException;
import com.koraei.parking.service.mapper.ParkingMapper;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ParkingServiceImpl implements ParkingService {

  @Autowired
  ParkingRepository parkingRepository;

  @Autowired
  VehicleService vehicleService;

  @Autowired
  TimeService timeService;

  @Autowired
  PriceRateService priceRateService;

  @Autowired
  TransactionService transactionService;

  @Override
  @Transactional
  public ParkingResponse insertAndUpdate(String carTag) {

    VehicleResponse vehicle = vehicleService.findByCarTag(carTag);
    if (vehicle == null) {
      vehicleService.add(
          carTag,
          null,
          null);
    }

    Parking parking = parkingRepository.findVehicleLogIn(carTag);

    if (parking == null) {
      return insert(carTag);
    } else {
      return update(carTag);
    }

  }

  @Override
  public List<ParkingResponse> getAllVehicleReport(String carTag, LocalDateTime logIn) {
    List<ParkingResponse> result = new ArrayList<>();
    PriceRate priceRate = priceRateService.findActivePrice();

    List<Parking> parkings = parkingRepository.findAllVehicleReport(carTag, logIn);

    if (parkings == null)
      throw new NotFoundException();

    parkings.forEach(parking -> {
      result.add(ParkingMapper.toResponse(
          parking,
          priceRate.getHourly(),
          calculateMinusTime(parking.getLogInDate(), parking.getLogOutDate())));
    });

    return result;
  }

  @Override
  public Parking getByIdObject(long id) {
    return parkingRepository.findById(id);
  }

  private ParkingResponse insert(String carTag) {

    Vehicle vehicle = vehicleService.findByCarTagObject(carTag);
    PriceRate priceRate = priceRateService.findActivePrice();

    Parking parking = new Parking(
        timeService.now(),
        null,
        new BigDecimal("0"),
        Payment.FAIL,
        vehicle,
        priceRate);

    parkingRepository.save(parking);

    return ParkingMapper.toResponse(parking, priceRate.getHourly(), null);

  }

  private ParkingResponse update(String carTag) {

    PriceRate priceRate = priceRateService.findActivePrice();

    Parking parking = parkingRepository.findVehicleLogIn(carTag);

    if (!transactionService.add(parking.getId()).getVerify()) {
      parking.setPayment(Payment.FAIL);
    } else {
      parking.setPayment(Payment.OK);
    }

    parking.setLogOutDate(timeService.now());
    BigDecimal price = calculatePrice(
        parking.getLogInDate(),
        parking.getLogOutDate(),
        parking.getPricaRate());
    parking.setPrice(price);
    parkingRepository.save(parking);
    return ParkingMapper.toResponse(
        parking,
        priceRate.getHourly(),
        calculateMinusTime(parking.getLogInDate(), parking.getLogOutDate()));
  }

  private BigDecimal calculatePrice(
      LocalDateTime logIn,
      LocalDateTime logOut,
      PriceRate priceRate) {

    TimeMinus parkTime = calculateMinusTime(logIn, logOut);
    BigDecimal hourly = new BigDecimal(
        String.valueOf(priceRate.getHourly().multiply(new BigDecimal(parkTime.getHour()))));

    BigDecimal daily = new BigDecimal(
        String.valueOf(priceRate.getDaily().multiply(new BigDecimal(parkTime.getDay()))));

    BigDecimal monthly = new BigDecimal(
        String.valueOf(priceRate.getMonthly().multiply(new BigDecimal(parkTime.getMonth()))));

    if (parkTime.getHour() == 0
        && parkTime.getDay() == 0
        && parkTime.getMonth() == 0) {
      hourly = new BigDecimal(
          String.valueOf(priceRate.getHourly().multiply(new BigDecimal("1"))));
    }

    return new BigDecimal(String.valueOf(hourly.add(daily).add(monthly)));
  }

  private TimeMinus calculateMinusTime(
      LocalDateTime logIn,
      LocalDateTime logOut) {
    long logInUnix = timeService.convertLocalDateTimeToUnix(logIn, ZoneOffset.UTC);
    long logOutUnix = timeService.convertLocalDateTimeToUnix(logOut, ZoneOffset.UTC);

    long hour = logIn.until(logOut, ChronoUnit.HOURS) % 24;
    long day = logIn.until(logOut, ChronoUnit.DAYS) % 30;
    long month = logIn.until(logOut, ChronoUnit.MONTHS);

    return new TimeMinus(hour, day, month);

  }

}
