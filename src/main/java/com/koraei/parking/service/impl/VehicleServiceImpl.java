package com.koraei.parking.service.impl;

import com.koraei.parking.core.entity.Vehicle;
import com.koraei.parking.core.repository.VehicleRepository;
import com.koraei.parking.service.VehicleService;
import com.koraei.parking.service.domain.VehicleResponse;
import com.koraei.parking.service.exception.NotFoundException;
import com.koraei.parking.service.exception.VehicleExistException;
import com.koraei.parking.service.mapper.VehicleMapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VehicleServiceImpl implements VehicleService {

  @Autowired
  VehicleRepository vehicleRepository;

  @Override
  @Transactional
  public VehicleResponse add(
      String car_tag,
      String ownerName,
      String model) {

    if (vehicleRepository.findByCarTag(car_tag) != null) {
      throw new VehicleExistException();
    }

    Vehicle vehicle = new Vehicle(
        car_tag,
        ownerName,
        model);

    return VehicleMapper.toResponse(vehicleRepository.save(vehicle));
  }

  @Override
  public VehicleResponse findByCarTag(String carTag) {
    Vehicle vehicle = vehicleRepository.findByCarTag(carTag);
    if (vehicle == null) {
      return null;
    }

    return VehicleMapper.toResponse(vehicle);
  }

  @Override
  public Vehicle findByCarTagObject(String carTag) {
    return vehicleRepository.findByCarTag(carTag);
  }

  @Override
  public List<VehicleResponse> findAll() {
    return null;
  }
}
