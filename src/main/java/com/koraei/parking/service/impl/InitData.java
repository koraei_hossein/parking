package com.koraei.parking.service.impl;

import com.koraei.parking.service.ParkingService;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class InitData {


  @Autowired
  ParkingService parkingService;

  @Scheduled(fixedRate = 1000, initialDelay = 1000)
  public void initMethodExampleBean() {

    parkingService.insertAndUpdate(generateRandomString(true, true, 8));

  }


  public String generateRandomString(boolean letter, boolean number, int length) {
    return RandomStringUtils.random(length, letter, number);
  }

}
